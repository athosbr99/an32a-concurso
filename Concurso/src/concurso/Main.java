/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 19 DE NOVEMBRO DE 2014
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
*/
package concurso;

/*
    Fonte das questões pré-gravadas:
       prova de algoritmos IFSC: http://www.ifc-camboriu.edu.br/~frozza/2012.1/IA12/Caderno%20de%20Exercicios%20-%20Algoritmos-v.1.3.pdf
*/

public class Main {
    public static void main(String[] args){
        Leitura ler = new Leitura();
        int menu = 0, menuQuestoes = 0, escolhaInscricao = 0;
        //Todos os vetores terão no máximo, 100 registros.
        
        // Endereços pré-gravados
        Endereco[] endereco = new Endereco[100];
        endereco[0] = new Endereco("Rua 13 de maio", 25,"86430-000","St. Ant. da Platina", "PR");
        endereco[1] = new Endereco("Rua 20 de agosto", 26,"86430-000","St. Ant. da Platina", "PR");
        endereco[2] = new Endereco("Rua das Flores",27,"86430-000","St. Ant. da Platina", "PR");
        endereco[3] = new Endereco("Rua Marechal Deodoro",27,"86430-000","St. Ant. da Platina", "PR");
        endereco[4] = new Endereco("Rua Ruy Barbosa", 28, "86430-000","St. Ant. da Platina", "PR");

        // Candidatos pré-gravados
        Candidato[] candidato = new Candidato[100];
        candidato[0] = new Candidato("Carlos Alberto de Nóbrega","341.851.662-90","111-222-333",endereco[0],30);
        candidato[1] = new Candidato("Jack Kerouac","964.710.876-16","9969-9969", endereco[1],31);
        candidato[2] = new Candidato("Bruce Wayne","748.083.344-02","4444-5555",endereco[2],32);
        candidato[3] = new Candidato("Luiz Hamilton Guidorizzi","557.464.965-96","8888-7777", endereco[3],33);
        candidato[4] = new Candidato("Ralph Costa Teixeira","785.374.160-02","2222-5555", endereco[4],34);
        Utilitario.idCandidato = 5;
        
        // Questões pré-gravadas
        Questao questao[] = new Questao[100];
        String ME1[] = {"72","48","45","39","38"};
        boolean ME1resp[] = {false,true,false,false,false};
        questao[0] = new ME(1,"Qual é o próximo número da sequência abaixo?\n18, 15, 30, 26, 42, 37, 54, ___",ME1,ME1resp);
        String ME2[] = {"hoje nao e segunda-feira e amanha nao chovera","hoje nao e segunda-feira ou amanha nao chovera","hoje nao e segunda-feira entao amanha chovera","hoje nao e segunda-feira nem amanha chovera","hoje e segunda feira e amanha chovera"};
        boolean ME2resp[] = {false, true, false, false, false};
        questao[1] = new ME(2,"Qual a negacao de \"hoje eh segunda-feira e amanha chovera\"?",ME2,ME2resp);
        questao[2] = new VF(3,"Responda verdadeiro ou falso","\'Pseudocodigo\' > \'turbo\'",true);
        String pergSoma1[] = {"teste = cod OU ( (x*2) <> soma)","x = soma","x = nome >= cor","cod = cor = \'verde\'","tudo = NAO teste OU cod E (soma < x)"};
        questao[3] = new Somatoria(4,"Conforme as declaracoes:\nreal soma,x;\nstring nome, cor;\nboolean cod, teste, tudo\n\nAssinale as atribuicoes validas",pergSoma1,17);
        questao[4] = new VF(5,"Considerando a afirmativa abaixo, reponda verdadeiro ou falso","O comando GOTO e uma palavra reservada em JAVA pois se refere a um comando de controle de fluxo",false);
        String ME3[] = {"!(p OU !q)","(p E q) OU (!p E !q)","p -> (q -> p)","(p OU !q)-> !q","p -> !q"};
        boolean ME3resp[] = {false,false,true,false,false};
        questao[5] = new ME(6,"Com relação as proposicoes abaixo, é TAUTOLOGIA apenas a proposicao em:",ME3,ME3resp);
        Utilitario.idQuestao = 6;
        
        Inscricao inscricao[] = new Inscricao[100];
        Concurso concurso[] = new Concurso[100];
        do {
            System.out.println("O que você deseja fazer?\n1 - Cadastrar Candidatos\n"
                    + "2 - Criar Questões\n3 - Criar Concursos\n4 - Criar Inscrições\n"
                    + "5 - Mostrar Candidatos Existentes\n6 - Mostrar Concursos Existentes\n"
                    + "7 - Mostrar Questões Existentes\n8 - Mostrar Inscrições Existentes\n"
                    + "9 - Pagar Inscrição\n10 - Corrigir Gabaritos\n11 - Excluir Inscrições\nEscolha: ");
            menu = ler.lerInt();
                switch (menu) {
                    case 1:
                        candidato[Utilitario.idCandidato] = Candidato.criar();
                        break;
                    case 2:
                        do {
                            System.out.println("Escolha o tipo de questão:\n1 - Multipla Escolha\n2 - Verdadeiro ou Falso\n3 - Somatoria\nEscolha: ");
                            menuQuestoes = ler.lerInt();
                            switch (menuQuestoes) {
                                case 1:
                                    questao[Utilitario.idQuestao] = ME.criar();
                                    break;
                                case 2:
                                    questao[Utilitario.idQuestao] = VF.criar();
                                    break;
                                case 3:
                                    questao[Utilitario.idQuestao] = Somatoria.criar();
                                    break;
                                default:
                                    System.out.println("Opção invalida.");
                                    break;
                            }
                            System.out.println("Pressione 1 pra sair. Pressione qualquer outro numero para criar outra questão.");
                            menuQuestoes = ler.lerInt();
                        } while (menuQuestoes != 1);
                        break;
                    case 3:
                        if (Utilitario.idQuestao == 0) {
                            System.out.println("Para fazer isso é necessário ter questões registradas. Tente novamente.");
                        } else {
                            concurso[Utilitario.idConcurso] = Concurso.criar(questao);
                        }
                        break;
                    case 4:
                        if (Utilitario.idCandidato == 0 || Utilitario.idConcurso == 0) {
                            System.out.println("Para registrar uma inscrição, é necessário um concurso e um candidato. Tente novamente.");
                        } else {
                            inscricao[Utilitario.controleInscricao] = Inscricao.criar(candidato, concurso);
                        }
                        break;
                    case 5:
                        if (Utilitario.idCandidato == 0) {
                            System.out.println("Para fazer isso é necessário ter candidatos registrados. Tente novamente.");
                        } else {
                            Utilitario.mostrarCandidatos(candidato);
                        }
                        break;
                    case 6:
                        if (Utilitario.idConcurso == 0) {
                            System.out.println("Para fazer isso é necessário ter concursos registrados. Tente novamente");
                        } else {
                            Utilitario.mostrarConcursos(concurso);
                        }
                        break;
                    case 7:
                        if (Utilitario.idQuestao == 0) {
                            System.out.println("Para fazer isso é necessário ter questões registradas. Tente novamente.");
                        } else {
                            Utilitario.mostrarQuestoes(questao);
                        }
                        break;
                    case 8:
                        if (Utilitario.controleInscricao == 0) {
                            System.out.println("Para fazer isso é necessário ter inscrições registradas. Tente novamente.");
                        } else {
                            Utilitario.mostrarInscricoes(inscricao);
                        }
                    break;
                    case 9:
                        if (Utilitario.controleInscricao == 0) {
                            System.out.println("Para fazer isso é necessário uma inscrição. Tente novamente."); 
                        } else {
                            Utilitario.pagarInscricao(inscricao);
                        }
                        break;
                    case 10:
                        if (Utilitario.controleInscricao == 0) {
                            System.out.println("Para fazer isso é necessário uma inscrição. Tente novamente.");
                        } else {
                            Utilitario.mostrarInscricoes(inscricao);
                            do {
                                System.out.println("Escolha a inscrição desejada: ");
                                escolhaInscricao = ler.lerInt();
                                escolhaInscricao--;
                            } while (escolhaInscricao < 0 || escolhaInscricao > Utilitario.controleInscricao);
                            if (inscricao[escolhaInscricao].getPaga()) {
                                inscricao[escolhaInscricao].corrigirGabarito();
                            } else {
                                System.out.println("Essa inscrição não está paga. Só é possivel corrigir inscrições que estão pagas.");
                            }
                        }
                        break;
                    case 11:
                        if (Utilitario.controleInscricao == 0) {
                            System.out.println("Para fazer isso é necessário uma inscrição. Tente novamente."); 
                        } else {
                            Inscricao.excluir(inscricao);
                        }
                        break;
                    default:
                        System.out.println("Opção invalida.");
                        break;
                }
                System.out.println("Digite 1 para sair. Digite qualquer outro numero para voltar ao menu.");
                menu = ler.lerInt();
        } while (menu != 1);  
    }   
}