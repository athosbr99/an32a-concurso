/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 19 DE NOVEMBRO DE 2014
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
    GILIARD ALMEIDA DE GODOI
*/

package concurso;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Concurso {
    private String titulo;
    private Calendar data;
    private Questao[] questoes;
    
    public Concurso(String t, Calendar d, Questao[] q){
        this.titulo = t;
        this.data = d;
        this.questoes = q;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Calendar getData() {
        return data;
    }

    public void setData(Calendar data) {
        this.data = data;
    }

    public Questao[] getQuestoes() {
        return questoes;
    }

    public void setQuestoes(Questao[] questoes) {
        this.questoes = questoes;
    }
    // Metodo que cria o objeto Concurso e o retorna para quem o chamou
    public static Concurso criar(Questao vet[]){
        String titulo;
        int dia, mes, ano, indiceEscolhido = 0, i;
        Leitura ler = new Leitura();
        System.out.println("Insira o titulo do seu concurso novo: ");
        titulo = ler.lerString();
        // Controla se o usuário inseriu um dia valido.
        do {
            System.out.println("Insira o dia da prova: ");
            dia = ler.lerInt();
        } while (dia < 0 && dia > 31);
        // Controla se o usuário inseriu um mês valido.
        do {
            System.out.println("Insira o mês da prova: ");
            mes = ler.lerInt();
        } while(mes < 1 || mes > 12);
        // A classe Calendar utiliza mes de 0 a 11.
        mes--;
        // Controla se o usuário inseriu um ano valido.
        do {
            System.out.println("Insira o ano da prova: ");
            ano = ler.lerInt();
        } while (ano < 2014);
        // Mostra todas as questões disponiveis para o usuário.
        Utilitario.mostrarQuestoes(vet);
        // Controla se o usuário inseriu um numero de questões válido.
        do {
            System.out.println("Quantas questões você usará neste concurso? Você pode escolher de 2 a 10: ");
            i = ler.lerInt();
        } while (i < 2 || i > 10);
        // O usuário escolhe as questões que ele quer.
        Questao[] vetEscolhidas = new Questao[i];
        for (int j = 0; j < i; j++) {
            do {
                System.out.printf("Insira a %d questão: ", (j+1));
                indiceEscolhido = ler.lerInt();
                indiceEscolhido--;
            } while (indiceEscolhido < 0 || indiceEscolhido > Utilitario.idQuestao);
            vetEscolhidas[j] = vet[indiceEscolhido]; 
        }
        vetEscolhidas[0].getEnunciado();
        // Grava a data que o usuário inseriu no método calendário.       
        Calendar data = new GregorianCalendar(ano,mes,dia);
        // Cria e retorna o objeto.
        Concurso concurso = new Concurso(titulo, data, vetEscolhidas);
        Utilitario.idConcurso++;
        return concurso;
    }

    @Override
    public String toString() {
        return "Titulo: " + this.titulo + "\nData: " + this.data.get(Calendar.DAY_OF_MONTH) + "/" + (this.data.get(Calendar.MONTH)+1) + "/" + this.data.get(Calendar.YEAR);
    }
}
