/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 19 DE NOVEMBRO DE 2014
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
*/
package concurso;

public class Somatoria extends Questao {
    private String pergunta[];
    private int resposta;
    
    // Criação do construtor do metodo.
    public Somatoria (int id, String enun, String pergunta[], int resposta) {
        super(id, enun);
        this.pergunta = pergunta;
        this.resposta = resposta;
    }
    
    // Método utilizado para corrigir a questão.
    public boolean corrigir(String resp){
        // Retorna se a resposta é verdadeira ou falsa.
        return resp.equals(getResposta() + "");
    }

    // Método para criar a questão. 
    public static Somatoria criar(){
        // Cria um objeto para leitura.
        Leitura leia = new Leitura();
        // Armazena o numero de questões, a resposta e o ID, respectivamente.
        // Questões de somatória possui 5 alternativas com valores (1 / 2 / 4 / 8 / 16) respectivamente.
        int numQuest, resposta = 0, id;
        numQuest = 5;
        String pergunta[] = new String[numQuest];
        String enun, auxResp;
               
        System.out.println("Informe o enunciado da questão: ");
        enun = leia.lerString();
        
        // Popula os valores para pergunta[]
        // Obtem a somatória para uma questão correta
        // Cada alternativa assume um valor que é potência de 2, isto é, 1, 2, 4, 8, 16
        // assim, se a alternativa estiver correta soma-se o valor dela na resposta final
        for (int i = 0; i < numQuest; i++){
            System.out.printf("Informe a pergunta da %d alternativa: ", i + 1);
            pergunta[i] = leia.lerString();
            System.out.println("Resposta (verdadeiro ou falso): ");
            auxResp = leia.lerString();
            if (auxResp.equalsIgnoreCase("verdadeiro")){
                resposta += (int)Math.pow(2,i);
            } 
        }                 
        //o id das questões é atribuido de forma incremental
        id = Utilitario.idQuestao;
        Utilitario.idQuestao++;
        //... e retorna eles para quem chamar Somatoria.criar()
        Somatoria soma = new Somatoria(id, enun, pergunta, resposta);
        return soma;
    }

    public String[] getPergunta() {
        return pergunta;
    }

    public void setPergunta(String[] pergunta) {
        this.pergunta = pergunta;
    }

    public int getResposta() {
        return resposta;
    }

    public void setResposta(int resposta) {
        this.resposta = resposta;
    }
    public String toString(){
        String aux;
        aux = super.toString()+"\n\n";
        for(int i = 0; i < this.pergunta.length; i++){
            aux += ("  "+((int)Math.pow(2,i))+" - "+this.pergunta[i]+"\n");
        }
        return aux;
    }
}