/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 20 DE NOVEMBRO DE 2014
    GILIARD ALMEIDA DE GODOI
*/

package concurso;

public class Utilitario {
    //Variavel utilizada para controlar as inscrições existentes.
    public static int controleInscricao;
    //Variavel utilizada para incrementar o id das questões quando elas são criadas.
    public static int idQuestao;
    //Variavel utilizada para controlar os candidatos existentes.
    public static int idCandidato;
    //Variavel utilizada para controlar os concursos existentes;
    public static int idConcurso;
    
    
    public static boolean validarCEP(String cep){
         /*
        REGRAS CONSIDERADAS PARA VALIDAR UM CEP:
            - O cep só pode conter números 
            - deve conter 8 digítos numéricos
            - o usuário poderá digitar no máximo um caracter ponto ' . '  e no máximo um caracter hífen ' - '
        */
        int k;
        k = cep.indexOf("-");
        if (k > 0) {
            cep = cep.substring(0,k)+ cep.substring(k+1);
        }
        k = cep.indexOf(".");
        if (k > 0) {
            cep = cep.substring(0, k) + cep.substring(k+1);
        }
        if (cep.length() != 8) {
            return false;
        }
        for (k = 0; k < cep.length(); k++){
            if(!Character.isDigit(cep.charAt(k))){
                return false;
            }
        }
        return true;
        
    }
    
    public static boolean validarCPF(String cpf){
      if (cpf.length() == 11 ){
          int    d1, d2; 
          int    digito1, digito2, resto; 
          int    digitoCPF; 
          String  nDigResult; 
          d1 = d2 = 0; 
          digito1 = digito2 = resto = 0; 
          for (int nCount = 1; nCount < cpf.length() -1; nCount++) { 
              digitoCPF = Integer.parseInt(cpf.substring(nCount -1, nCount)); 
              // Multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4 e assim por diante. 
              d1 = d1 + ( 11 - nCount ) * digitoCPF; 
              // Para o segundo digito repita o procedimento incluindo o primeiro digito calculado no passo anterior. 
              d2 = d2 + ( 12 - nCount ) * digitoCPF; 
          }
          // Primeiro resto da divisão por 11. 
          resto = (d1 % 11); 
          // Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior. 
          if (resto < 2)
              digito1 = 0;
          else 
              digito1 = 11 - resto; 
          d2 += 2 * digito1; 
          // Segundo resto da divisão por 11. 
          resto = (d2 % 11); 
          // Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior. 
          if (resto < 2) 
              digito2 = 0; 
          else 
              digito2 = 11 - resto; 
          // Digito verificador do CPF que está sendo validado. 
          String nDigVerific = cpf.substring (cpf.length()-2, cpf.length()); 
          // Concatenando o primeiro resto com o segundo. 
          nDigResult = String.valueOf(digito1) + String.valueOf(digito2); 
          // Comparar o digito verificador do cpf com o primeiro resto + o segundo resto. 
          return nDigVerific.equals(nDigResult); 
        }
        return false;
    }
    
    // Metodo utilizado para validação do estado inserido pelo usuário.
    public static boolean validarEstado(String uf){
        String vet[] = {"AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
        for (int k = 0 ; k < 27 ; k++){
            // Verifica se o que o usuário inserou está dentro do vetor de siglas.
            if (uf.equals(vet[k])){
                return true;
            }
        }
        return false;
    }
    
    // Método para pagar as inscrições de um candidato
    public static void pagarInscricao(Inscricao inscricao[]){
        Leitura ler = new Leitura();
        int idPagar;
        Utilitario.mostrarInscricoes(inscricao);
        do {
            System.out.println("Digite o ID que você deseja pagar a inscrição: ");
            idPagar = ler.lerInt();
            idPagar--;
        } while (idPagar < 0 || idPagar > Utilitario.controleInscricao);
        inscricao[idPagar].setPaga(true);
    }
    
    // Metodo para traduzir "verdadeiro" ou "falso" de String para true ou false boolean
    public static boolean tradutorLogico(String resp) {
        if (resp.equalsIgnoreCase("verdadeiro")) {
            return true;
        } else if (resp.equalsIgnoreCase("falso")) {
            return false;
        } else {
            System.out.println("O usuário inseriu uma resposta invalida.");
            return false;
        }
    }
    
    // Metodo para mostrar todos os candidatos.
    public static void mostrarCandidatos(Candidato cand[]) {
        for (int i = 0; i < Utilitario.idCandidato; i++) {
            System.out.println((i + 1) + " - " + cand[i].getNome());
        }
    }
    
    // Metodo para mostrar todas as questões.
    public static void mostrarQuestoes(Questao q[]) {
        for (int i = 0; i < Utilitario.idQuestao; i++) {
            System.out.println((i + 1) + " - " + q[i].toString());
        }
    }
    
    // Metodo para mostrar todos os concursos.
    public static void mostrarConcursos(Concurso c[]) {
        for (int i = 0; i < Utilitario.idConcurso; i++) {
            System.out.println((i + 1) + " - " + c[i].toString());
        }
    }
    
    // Metodo para mostrar todas as inscrições.
    public static void mostrarInscricoes(Inscricao in[]) {
        for (int i = 0; i < Utilitario.controleInscricao; i++) {
            System.out.println((i + 1) + " - " + in[i].toString());
        }
    }
}