/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 19 DE NOVEMBRO DE 2014
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
*/

package concurso;

public class ME extends Questao{
    private String pergunta[];
    private boolean resposta[];
    
    // Criação do construtor do método.
    public ME(int id, String enun, String pergunta[], boolean resposta[]){
        super(id, enun);
        this.pergunta = pergunta;
        this.resposta = resposta;
    }

    // Método para corrigir a questão. A melhorar.
    public boolean corrigir(String resp){
        switch (resp) {
            case "a":
                return resposta[0];
            case "b":
                return resposta[1];
            case "c":
                return resposta[2];
            case "d":
                return resposta[3];
            case "e":
                return resposta[4];
            default:
                System.out.println("Resposta invalida.");
                return false;
        }
    }

    // Metodo para criar a questão.
    public static Questao criar(){
        Leitura leia = new Leitura();
        int i, numPerg, numResp, id;
        // o numero de resposta é igual ao número de perguntas
        numPerg = numResp = 5;
        String enun, pergunta[];
        boolean verificacaoB, resposta[];
        //Captura o ID da questão. A ser melhorado?
        System.out.println("Informe o enunciado da questão: ");
        enun = leia.lerString();
        pergunta = new String[numPerg];
        // um vetor do tipo boolean é inicializado por padrão com false
        resposta = new boolean[numResp];
        for (i = 0; i < numPerg; i++) {
            System.out.printf("Informe a pergunta da %d alternativa: ", i + 1);
            pergunta[i] = leia.lerString();
        }
        do {
            System.out.println("Qual das alternativas é verdadeira: ");
            i = leia.lerInt();
            if (i < 0 || i > 5) {
                System.out.println("Escolha uma alternativa entre 1 e 5\n");
            }
        } while(i < 0 || i > 5);
        i--;
        // um vetor do tipo boolean é inicializado por padrão com false
        // assim eu só preciso inseir true na posição da resposta
        resposta[i] = true;        
        // Retorna o objeto para quem o chamou.
        //o id das questões é atribuido de forma incremental
        id = Utilitario.idQuestao;
        Utilitario.idQuestao++;
        ME me = new ME(id, enun, pergunta, resposta);
        return me;
    }

    public String[] getPergunta() {
        return pergunta;
    }

    public void setPergunta(String[] pergunta) {
        this.pergunta = pergunta;
    }

    public boolean[] getResposta() {
        return resposta;
    }

    public void setResposta(boolean[] resposta) {
        this.resposta = resposta;
    }
    public String toString(){
        String vetLetra[] = {"a", "b", "c", "d", "e"};
        String aux;
        aux = super.toString()+"\n\n";
        for(int i = 0; i < this.pergunta.length; i++){
            aux += ("  " + vetLetra[i] + " - " + this.pergunta[i] + "\n");
        }        
        return aux;
    }
}