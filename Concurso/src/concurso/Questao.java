/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 19 DE NOVEMBRO DE 2014
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
*/
package concurso;

public class Questao{
    private int id;
    private String enunciado;
    
    public Questao(int id, String enunciado){
        this.id = id;
        this.enunciado = enunciado;
    }
    
    public Questao(){
        this.id = 0;
        this.enunciado = new String();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    // O metodo é sobreescrito, logo o metodo especificado abaixo não vai ser utilizado.
    public static Questao criar(){
        Questao t = new Questao();
        return t;
    }
    
    public boolean corrigir(String resposta){
        return true;
    }
    public String toString(){
        return this.enunciado;
    }  
}