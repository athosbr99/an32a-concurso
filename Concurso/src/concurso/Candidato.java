/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 20 DE NOVEMBRO DE 2014
    GILIARD ALMEIDA DE GODOI
*/

package concurso;

public class Candidato extends Pessoa {
    private int cod;
    
    // Criação do construtor do objeto.
    public Candidato(String nome, String cpf, String tel, Endereco end, int num){
        super(nome, cpf, tel, end);
        this.cod = num;
    }
    
    // Método para criar o objeto.
    public static Candidato criar(){
        String nome, cpf, tel;
        Endereco end;
        int num;
        // Objeto de leitura de dados.
        Leitura ler = new Leitura();
        
        System.out.printf("Nome: ");
        nome = ler.lerString();
        // Valida se o CPF inserido está correto.
        do {
            System.out.printf("CPF: ");
            cpf = ler.lerString();
        } while(!Utilitario.validarCPF(cpf));
        System.out.printf("Telefone: ");
        tel = ler.lerString(); 
        // Criação do objeto de endereço.
        end = Endereco.criar();        
        num = Utilitario.idCandidato;
        Utilitario.idCandidato++;
        // Retorna o objeto para quem o chamou.
        Candidato c = new Candidato(nome, cpf, tel, end, num);
        return c;
    }
    
    public void setCod(int num){
        this.cod = num;
    }
    
    public int getCod(){
        return this.cod;
    }
    
    public String toString(){
        return "ID "+cod+" - "+super.toString();
    }
    
}
