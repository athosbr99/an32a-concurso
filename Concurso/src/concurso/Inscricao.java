/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 19 DE NOVEMBRO DE 2014
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
    GILIARD ALMEIDA DE GODOI
*/
package concurso;

import java.util.Calendar;

public class Inscricao {
    private Candidato candidato;
    private Concurso concurso;
    private String[] gabarito;
    private double nota;
    private boolean paga;
    private Calendar dataInsc;
    
    /* Construtor sobregarregado de Inscrição
       Quando for construir uma nova inscrição eu não vou ter o gabarito e a nota do candidato
        esta informações eu só terei após a "realização do concurso"
        seria isso mesmo?
    */
    public Inscricao(Candidato candidato, Concurso concurso, Calendar data){
        this.candidato = candidato;
        this.concurso = concurso;
        this.paga = false;
        this.dataInsc = data;
        this.gabarito = new String[this.concurso.getQuestoes().length];
    }
    // Cria o objeto Inscrição e o retorna para quem o chamou.
    public static Inscricao criar(Candidato cand[], Concurso conc[]) {
        int indiceCandidato = -1, indiceConcurso = -1;
        Calendar data;
        Leitura ler = new Leitura();
        Utilitario.mostrarCandidatos(cand);
        do {
            System.out.println("Escolha o ID do candidato desejado: ");
            indiceCandidato = ler.lerInt();
            if (indiceCandidato < 0 || indiceCandidato > Utilitario.idCandidato) {
                System.out.println("Não existe esse candidato. Tente outro.");
            }
        } while (indiceCandidato < 0 || indiceCandidato > Utilitario.idCandidato);
        Utilitario.mostrarConcursos(conc);
        do {
            System.out.println("Insira o ID do concurso que deseja associar ao candidato: ");
            indiceConcurso = ler.lerInt();
            if (indiceConcurso < 0 || indiceConcurso > (Utilitario.idConcurso)) {
                System.out.println("Não existe esse concurso. Tente outro.");
            }
        } while (indiceConcurso < 0 || indiceConcurso > (Utilitario.idConcurso));
        // Grava a data que o usuário inseriu no método calendário, falta validação.
        data = Calendar.getInstance();
        Inscricao inscricao = new Inscricao(cand[indiceCandidato-1], conc[indiceConcurso-1], data);
        Utilitario.controleInscricao++;
        return inscricao;
    }
    	

    public static boolean excluir (Inscricao insc[]){
            int escolhaInscricao;
            Leitura ler = new Leitura();
            Utilitario.mostrarInscricoes(insc);
            do {
                System.out.println("Escolha a inscrição desejada (pressione -1 pra sair): ");
                escolhaInscricao = ler.lerInt();
                escolhaInscricao--;
            } while (escolhaInscricao < -1 || escolhaInscricao > Utilitario.controleInscricao);
            if (escolhaInscricao == -1) {
                return false;
            }
            insc[escolhaInscricao] = insc[Utilitario.controleInscricao - 1];
            insc[Utilitario.controleInscricao - 1] = null;
            Utilitario.controleInscricao--;
            return true;        
        }

    public void corrigirGabarito(){
        Leitura ler = new Leitura();
        for (int i = 0; i < this.concurso.getQuestoes().length; i++){
            System.out.print(this.concurso.getQuestoes()[i].toString());
            this.gabarito[i] = ler.lerString();
        }
        for (int i = 0; i < gabarito.length; i++){
            if (concurso.getQuestoes()[i].corrigir(gabarito[i])) {
                this.nota += 1;
            }
        }
        System.out.println("Sua nota foi: " + this.nota);
    }
    // Métodos acessores gets e sets
    public Candidato getCandidato() {
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    public Concurso getConcurso() {
        return concurso;
    }

    public void setConcurso(Concurso concurso) {
        this.concurso = concurso;
    }

    public String[] getGabarito() {
        return gabarito;
    }

    public void setGabarito(String[] gabarito) {
        this.gabarito = gabarito;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public boolean getPaga() {
        return paga;
    }

    public void setPaga(boolean paga) {
        this.paga = paga;
    }

    public Calendar getDataInsc() {
        return dataInsc;
    }

    public void setDataInsc(Calendar dataInsc) {
        this.dataInsc = dataInsc;
    }

    @Override
    public String toString() {
        return "Nome: " + this.candidato.getNome() + "\nConcurso: " + this.concurso.getTitulo() + "\nData da Inscrição: " + this.dataInsc.get(Calendar.DAY_OF_MONTH) + "/" + (this.dataInsc.get(Calendar.MONTH)+1) + "/" + this.dataInsc.get(Calendar.YEAR);
    }
    
}