/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 20 DE NOVEMBRO DE 2014
    GILIARD ALMEIDA DE GODOI
*/

package concurso;

public class Pessoa {
    private String nome;
    private String cpf;
    private String fone;
    private Endereco endereco;
    
    public Pessoa(String nome, String cpf, String fone, Endereco end){
        this.nome = nome;
        this.cpf = cpf;
        this.fone = fone;
        this.endereco = end;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    public String toString(){
        return "Nome: " + nome + " | CPF: " + cpf + " | TEL: " + fone + "\nEndereco: " + endereco.toString();
    }
}
