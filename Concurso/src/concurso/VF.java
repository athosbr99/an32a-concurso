/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 19 DE NOVEMBRO DE 2014
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
*/
package concurso;

public class VF extends Questao {
    private String pergunta;
    private boolean resposta;
    
    // Criação do construtor do método.
    public VF (int id, String enun, String pergunta, boolean resposta) {
        super(id, enun);
        this.pergunta = pergunta;
        this.resposta = resposta;
    }
    
    // Metodo utilizado para corrigir a questão.
    public boolean corrigir (String resp){
        return Utilitario.tradutorLogico(resp) == resposta;
    }

    // Metodo utilizado para criar a questão.
    public static Questao criar() {
        // Variavel utilizada para verificação da resposta verdadeiro/falso inserida pelo usuário.
        boolean verificacao;
        // Variavel utilizada para armazenar a resposta da questão.
        boolean resposta = false;
        // Cria um objeto para leitura.
        Leitura leia = new Leitura();
        // Variaveis utilizadas para armazenar enunciado e pergunta.
        String enun, pergunta;
        // Variavel utilizada para armazenar o ID da questão.
        int id;
    
        System.out.println("Informe o enunciado da questão: ");
        enun = leia.lerString();
            
        System.out.println("Informe a pergunta: ");
        pergunta = leia.lerString();
            
        // "Converte" a resposta do usuário em verdadeiro/falso para comparação.
        do {
            System.out.println("Informe a resposta (veradeiro / falso): ");
            String resp = leia.lerString();
            if (resp.equalsIgnoreCase("verdadeiro")) {
                resposta = true;
                verificacao = true;
            } else if (resp.equalsIgnoreCase("falso")) {
                resposta = false;
                verificacao = true;
            } else {
                System.out.println("Valor invalido. Escreva verdadeiro ou falso.");
                verificacao = false;
            }
        // Permanece no loop enquanto o usuário não insere um valor válido.
        } while (verificacao == false);
        // Cria o objeto e o retorna para quem o chamou.
        //o id das questões é atribuido de forma incremental
        id = Utilitario.idQuestao;
        Utilitario.idQuestao++;
        VF vf = new VF(id, enun, pergunta, resposta);
        return vf;
    }

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public boolean isResposta() {
        return resposta;
    }

    public void setResposta(boolean resposta) {
        this.resposta = resposta;
    }
    public String toString(){
        return super.toString() + "\n" + this.pergunta + "\n";
    }
}