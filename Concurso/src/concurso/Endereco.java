/*
    PROJETO CONCURSO
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    IMPLEMENTAÇÃO INICIAL: 20 DE NOVEMBRO DE 2014
    GILIARD ALMEIDA DE GODOI
*/

package concurso;

public class Endereco {
    private String rua;
    private int numero;
    private String cep;
    private String cidade;
    private String estado;
    
    public Endereco(String rua, int num, String cep, String cid, String est){
        this.rua = rua;
        this.numero = num;
        this.cep = cep;
        this.cidade = cid;
        this.estado = est;
    }
    
    public static Endereco criar(){
        String r, cep, cid, est;
        int num;
        Endereco end;
        Leitura ler = new Leitura();
        System.out.printf("Digite o nome da rua: ");
        r = ler.lerString();
        System.out.printf("Digite o numero: ");
        num = ler.lerInt();
        System.out.printf("Digite o nome da cidade: ");
        cid = ler.lerString();
        do {
            System.out.printf("Digite o CEP: ");
            cep = ler.lerString();            
        } while(!Utilitario.validarCEP(cep));
        do {
            System.out.printf("Digite o estado (SIGLA): ");
            est = ler.lerString();           
        } while(!Utilitario.validarEstado(est));       
        end = new Endereco(r,num,cep,cid, est);
        return end;    
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String r) {
        this.rua = r;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int num) {
        this.numero = num;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cid) {
        this.cidade = cid;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public String toString(){
        return ("Rua: " + rua + "\nNumero: " + numero + "\nCEP: " + cep + "\nCidade: " + cidade + "\nEstado: " + estado);
    }
}
