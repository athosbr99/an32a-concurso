/*
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
    GILIARD GODOI DE ALMEIDA
*/

package conversor;

import junit.framework.Test;
import junit.framework.TestCase;

public class FahrenheitTeste extends TestCase implements Test{
    public void testCelsius(){
        //Teste da equação
        double entrada[] = {0.0, 0.0};
        double retEsperado[] = {-273.0, 394.0};
        for (int i = 0; i < entrada.length; i++) {
            double retorno = Kelvin.Celsius(entrada[i]);
            assertEquals(retEsperado[i], retorno, 0);
        }
    }
    
    public void testKelvin(){
        double entrada[] = {0.0, 0.0};
        double retEsperado[] = {12.0, 293,0};
        for (int i = 0; i < entrada.length; i++) {
            double retorno = Kelvin.Fahrenheit(entrada[i]);
            assertEquals(retEsperado[i], retorno, 0);
        }
    }
}
