/*
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
    GILIARD GODOI DE ALMEIDA
*/

package conversor;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class UtilitarioTeste {
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testIdentificarUnidadeCelsius(){
        String unidade = "32 C";
        int resultadoEsperado = 0;
        int id = Utilitario.identificarUnidade(unidade);
        assertEquals(resultadoEsperado,id,0);
    }
    
    @Test
    public void testIdentificarUnidadeKelvin(){
        String und = "273K";
        int resultadoEsperado = 2;
        int id = Utilitario.identificarUnidade(und);
        assertEquals(resultadoEsperado,id,0);
        
    }
    
    @Test
    public void testIdentificarUnidadeFahrenheit(){
        String und = "273F";
        int resultadoEsperado = 1;
        int id = Utilitario.identificarUnidade(und);
        assertEquals(resultadoEsperado,id,0);
    }
    
    @Test
    public void testIdentificarMedida(){
        String und = "273K";
        double resultadoEsperado = 273.0;
        double resultado = Utilitario.identificarMedida(und);
        assertEquals(resultadoEsperado,resultado,0);
    }
    
    @Test
    public void testIdentificarOpcao(){
        int unidOrigem = 2;
        int opcaoUsuario = 1;
        int resultado = Utilitario.identificarOpcao(unidOrigem, opcaoUsuario);
        int resultadoEsperado = 21;
        assertEquals(resultadoEsperado,resultado,0);
    }
}
