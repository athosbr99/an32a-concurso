/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

import junit.framework.*;
public class CelsiusTeste extends TestCase implements Test{
    public void testKelvin(){
        //Teste da equação
        
        double entrada[] = {0.0};
        double retEsperado[] = {-273.0};
        for (int i = 0; i < entrada.length; i++) {
            double retorno = Celsius.Kelvin(entrada[i]);
            assertEquals(retEsperado[i], retorno, 0);
        }
        
        
    }
}
