/*
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
    GILIARD GODOI DE ALMEIDA
*/

package conversor;

import junit.framework.Test;
import junit.framework.TestCase;

public class KelvinTeste extends TestCase implements Test {
    public void testFahrenheit(){
        //Teste da equação
        double entrada[] = {0.0, 0.0};
        double retEsperado[] = {-273.0, 394.0};
        for (int i = 0; i < entrada.length; i++) {
            double retorno = Kelvin.Fahrenheit(entrada[i]);
            assertEquals(retEsperado[i], retorno, 0);
        }
    }
    
    public void testCelsius(){
        //Teste da equação
        double entrada[] = {0.0, 0.0};
        double retEsperado[] = {-273.0, 394.0};
        for (int i = 0; i < entrada.length; i++) {
            double retorno = Kelvin.Celsius(entrada[i]);
            assertEquals(retEsperado[i], retorno, 0);
        }
    }
}
