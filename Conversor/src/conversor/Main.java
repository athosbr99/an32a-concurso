/*
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
    GILIARD GODOI DE ALMEIDA
*/

package conversor;

public class Main {

    public static void main(String[] args) {
       String unidTemperatura[] = {"Celsius (°C)",
                                    "Fahrenheit (°F)",
                                    "Kelvin (K)"};
       String aux;
       double medida;
       int und = -1, op, k = 1;
       Leitura ler = new Leitura();
        System.out.println("** CALCULADORA PARA CONVERSÃO DE TEMPERATURA **");
        // leitura de uma string do usuário que contenha 
        do{
            System.out.print("\nQual a temperatura atual?\n->  ");
            aux = ler.lerString();
            aux = aux.replaceAll(" ", "");
            und = Utilitario.identificarUnidade(aux);
        }while(und == -1);
        System.out.println(und);
        // vai ler a string e converter num double para realizar a conversão
        medida = Utilitario.identificarMedida(aux);
        // esse for é o responsável por escrever as opções do usuário na tela com base na informação da unidade de temperatura de origem
        System.out.println("Deseja converter "+aux+" para: ");
        for(int i = 0; i < unidTemperatura.length; i++){
            if(und != i){
                System.out.println("\t"+k+" - "+unidTemperatura[i]);
                k++;
            }
        }
        do{
            System.out.print("-> ");
            op = ler.lerInt();
        }while(op < 1 || op > 2);
        op = Utilitario.identificarOpcao(und, op);
        // System.out.println(op); linha usada para verificar se a saída vai ser uma das que estão prevista no switch 
        switch(op){
            case 1:
                // transforma de Celsius para Fahrenheit
                medida = Fahrenheit.Celsius(medida);
                k = 1;
                break;
            case 2:
                // transforma de Celsius para Kelvin
                medida = Kelvin.Celsius(medida);
                k = 2;
                break;
            case 11:
                // transforma de Fahrenheit para Celsius
                medida = Celsius.Fahrenheit(medida);
                k = 0;
                break;
            case 12:
                // transforma de Fahrenheit para Kelvin
                medida = Kelvin.Fahrenheit(medida);
                k = 2;
                break;
            case 21:
                // transforma de Kelvin para Celsius
                medida = Celsius.Kelvin(medida);
                k = 0;
                break;
            case 22:
                // transforma de Kelvin para Fahrenheit
                medida = Fahrenheit.Kelvin(medida);
                k = 1;
                break;
            default :
                System.out.println("Algum erro ocorreu!!\nVERIFIQUE...");
        }
        
        System.out.println(aux + " equivale a " + medida + " " + unidTemperatura[k]);
    }
    
}
