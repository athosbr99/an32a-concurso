/*
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
    GILIARD GODOI DE ALMEIDA
*/

package conversor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Leitura {
    InputStreamReader in = new InputStreamReader(System.in);
    BufferedReader input = new BufferedReader(in);
    
    // Classe para ler inteiros, sendo que a conversão já é feita pela própria classe.
    public int lerInt() {
        while (true) {
            try {
                return Integer.parseInt(input.readLine());
            } catch (IOException e) {
                System.out.println("Falha critica.");
            } catch (NumberFormatException e) {
                System.out.println("Você digitou uma entrada invalida.");
        }
    }
}
    
    // Classe para ler strings. Não é necessária conversão.
    public String lerString() {
        while (true) {
            try {
                return input.readLine();
            } catch (IOException e) {
                System.out.println("Falha critica.");
            }
        }
    }
    
    // Classe para ler valores de ponto flutuante, sendo que a conversão já é feita pela própria classe.
    public double lerDouble() {
        while (true) {
            try {
                return Double.parseDouble(input.readLine());
            } catch (IOException e) {
                System.out.println("Falha critica.");
            } catch (NumberFormatException e) {
                System.out.println("Você digitou uma entrada invalida.");
            }
        }
    }
}
