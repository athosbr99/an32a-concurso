package conversor;

public class Celsius {
    public static double Fahrenheit (double f) {
        return ((f-32)*5)/9;
    }
    
    public static double Kelvin (double k) {
        return k-273;
    }
}
