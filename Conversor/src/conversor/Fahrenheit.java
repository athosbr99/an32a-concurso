package conversor;

public class Fahrenheit {
    public static double Celsius (double c) {
        return 1.8 * c + 32;
    }
    
    public static double Kelvin (double k) {
        return ((k - 273.15) * 1.8) + 32;
    }
}
