package conversor;

public class Kelvin {
   
    public static double Celsius (double c) {
        return c + 273;
    }
    
    public static double Fahrenheit (double f) {
        double c = Celsius.Fahrenheit(f);
        return Celsius(c);
    }
}
