/*
    UNIVERSIDADE TECNOLÓGICA FEDERAL DO PARANÁ
    RICARDO CARREIRO ALVIM
    ATHOS CASTRO MORENO
    GILIARD GODOI DE ALMEIDA
*/

package conversor;

public class Utilitario {
    private static String unidades[] = {"C","F","K"};
    
    public static int identificarUnidade(String value){
        
        int i = 0, index = -1;
        value = value.toUpperCase();
        value = value.replaceAll(" ", "");
        while( i < unidades.length && index == -1){
            index = value.indexOf(unidades[i]);
            i++;
        }
        if(i == unidades.length && index == -1){
            return -1;
        }else{
            i--;
            return i;
        }
    }
    
    public static double identificarMedida(String value){
        int i;
        String aux = "";
        double medida;
        i = identificarUnidade(value);
        if( i == -1){
            return 0.0;
        }
        value = value.replaceAll(unidades[i],""); // substitui a unidade de medida por uma string vazia, isto é, por nada
        value = value.replaceAll(" ", ""); // substitui todos os espaços em branço por nada, isto é, um String vazia
        for(int j = 0; j < value.length(); j++){
            if(Character.isDigit(value.charAt(j))){
                aux+= value.charAt(j);
                
            }else if(','== value.charAt(j) || '.' == value.charAt(j)){
                    aux += ".";
            }  
        }
        try{
            medida = Double.parseDouble(aux);
        }catch(NumberFormatException e){
            System.out.println("Digite sua medida novamente de forma correta!");
            return 0.0;
        }
        return medida;
    }
    
    public static int identificarOpcao(int unidOrigem, int opcaoUsuario){
        return (unidOrigem*10)+opcaoUsuario;
    }
    
    
}
